const express = require("express")
const path = require('path')
const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const cookieParser = require('cookie-parser')
app.use(cookieParser())

// /* Starting the server on PORT 4001 */
// const PORT = 4001
// app.listen(PORT, () => {
//     console.log(`App running on PORT ${PORT} ..`)
// })

app.use(express.json())
app.use('/api/v1/users', userRouter)
app.use('/', viewRouter)

app.use(express.static(path.join(__dirname, 'views')))
module.exports = app